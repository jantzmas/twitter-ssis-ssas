IF EXISTS ( SELECT  *
            FROM    sys.objects
            WHERE   object_id = OBJECT_ID(N'[dbo].[usp_Populate_Fact_Twitter_Feeds]')
                    AND type IN ( N'P', N'PC' ) ) 
    DROP PROCEDURE [dbo].[usp_Populate_Fact_Twitter_Feeds]
GO
CREATE PROCEDURE usp_Populate_Fact_Twitter_Feeds
AS 
    BEGIN
        BEGIN TRY

            INSERT  INTO [Twitter_DB].[dbo].[Fact_Twitter_Feeds]
                    ( Twitter_ID ,
                      Twitter_Feed ,
                      Author ,
                      Author_Email ,
                      FK_Sentiment ,
                      FK_Time ,
                      FK_Date ,
                      FK_Feed
                    )
                    SELECT  tfsd.Twitter_ID ,
                            tfsd.Twitter_Feed ,
                            tfsd.Author ,
                            tfsd.Author_Email ,
                            ISNULL(s.Sentiment_Key, -1) ,
                            ISNULL(t.Time_Key, -1) ,
                            ISNULL(d.Date_Key, -1) ,
                            ISNULL(f.Feed_Key, -1)
                    FROM    [Twitter_DB].[dbo].[Twitter_Feeds_Staging_Data] AS tfsd
                            LEFT JOIN [Twitter_DB].[dbo].[Dim_Sentiment] s ON tfsd.Sentiment = s.Sentiment
                            LEFT JOIN [Twitter_DB].[dbo].[Dim_Time] t ON SUBSTRING(CONVERT(VARCHAR, CONVERT(TIME, tfsd.Twitter_Date), 108),
                                                              0, 6) + ':00' = t.Time_Key_Alternate
                            LEFT JOIN [Twitter_DB].[dbo].[Dim_Date] d ON CAST(CONVERT(CHAR(8), CAST(tfsd.Twitter_Date AS DATETIME), 112) AS INT) = d.Date_Key_Alternate
                            LEFT JOIN [Twitter_DB].[dbo].[Dim_Feed] f ON tfsd.Feed_ID = f.Feed_Key
                    WHERE   NOT EXISTS ( SELECT TOP 1
                                                'I already exist'
                                         FROM   [Twitter_DB].[dbo].[Fact_Twitter_Feeds] fact
                                         WHERE  fact.Twitter_ID = tfsd.Twitter_ID
                                                AND fact.Twitter_Feed = tfsd.Twitter_Feed
                                                AND fact.Author = tfsd.Author
                                                AND fact.Author_Email = tfsd.Author_Email )
        END TRY

        BEGIN CATCH
            IF @@TRANCOUNT > 0 
                BEGIN
                    ROLLBACK TRANSACTION
                END	

            DECLARE @ErrorMessage NVARCHAR(4000);
            DECLARE @ErrorSeverity INT;
            DECLARE @ErrorState INT;

            SELECT  @ErrorMessage = ERROR_MESSAGE() ,
                    @ErrorSeverity = ERROR_SEVERITY() ,
                    @ErrorState = ERROR_STATE();
			
            RAISERROR (@ErrorMessage, -- Message text.
				   @ErrorSeverity, -- Severity.
				   @ErrorState -- State.
				   );
        END CATCH

        IF @@TRANCOUNT > 0 
            BEGIN
                COMMIT TRANSACTION
            END
    END