-----------------------------------------
--	POPULATE 'Dim_Time' DIMENSION TABLE	|
-----------------------------------------

USE Twitter_DB
DECLARE @Time DATETIME 
SET @TIME = CONVERT(VARCHAR,'12:00:00 AM',108) 

SET NOCOUNT ON
SET ANSI_PADDING OFF 

WHILE @TIME <= '11:59:59 PM' 
 BEGIN 
    
    INSERT INTO dbo.dim_Time ([Time_Key_Alternate], [Hour],  [Minute],  [Am_Pm]) 
    SELECT	CONVERT(VARCHAR,@TIME,108) [Time_Key_Alternate], 
			CASE WHEN DATEPART(HOUR,@Time) > 12 THEN DATEPART(HOUR,@Time) - 12 ELSE DATEPART(HOUR,@Time) END AS [Hour], 
			DATEPART(MINUTE,@Time) [Minute], 
			CASE WHEN DATEPART(HOUR,@Time) >= 12 THEN 'PM' ELSE 'AM' END AS [AmPm] 

    SELECT @TIME = DATEADD(minute,1,@Time) 
 END 

UPDATE dim_Time 
SET [HOUR] = '0' + [HOUR] 
WHERE LEN([HOUR]) = 1 

UPDATE dim_Time 
SET [MINUTE] = '0' + [MINUTE] 
WHERE LEN([MINUTE]) = 1 

UPDATE dim_Time 
SET Standard_Time = [Hour] + ':' + [Minute] + ' ' + Am_Pm 
WHERE Standard_Time IS NULL AND HOUR <> '00' 

UPDATE dim_Time 
SET Standard_Time = '12' + ':' + [Minute] + ' ' + Am_Pm 
WHERE [HOUR] = '00' 

UPDATE
[Twitter_DB].[dbo].[Dim_Time]
SET [hour] = CASE WHEN [am_pm] = 'PM' THEN SUBSTRING(Time_Key_Alternate,0,3) ELSE [Hour] END
FROM [Twitter_DB].[dbo].[Dim_Time]


-----------------------------------------
--	POPULATE 'Dim_Date' DIMENSION TABLE	|
-----------------------------------------

USE Twitter_DB

DECLARE @Start_Day		DATE = DATEADD(MONTH,-1,GETDATE())
DECLARE @End_Day		DATE = DATEADD(MONTH,1, GetDate());

WITH DateCTE AS
(
  SELECT CAST(@Start_Day AS DATETIME) AS Date_Value
  UNION ALL
  SELECT DATEADD(dd, 1, Date_Value)
  FROM DateCTE
  WHERE DATEADD(dd, 1, Date_Value) <= @End_Day
)
 
INSERT INTO dbo.Dim_Date
	SELECT 
	[Date_Key_Alternate]			=	CAST(CONVERT(CHAR(8),CAST(Date_Value as DATETIME),112) as INT),
	[Date_Time]						=	Date_Value,
	[Calendar_Date]					=	CAST(Date_Value as DATE),
	[Week_Day_Name]					=	DATENAME(dw, Date_Value),
	[Calendar_Year]					=	YEAR(Date_Value),
	[Calendar_Quarter]				=	CAST(DATEPART(Quarter ,Date_Value) AS CHAR(1)),
	[Calendar_Quarter_Name]			=	'Quarter ' + CAST(DATEPART(QUARTER, Date_Value) AS CHAR(1)),
	[Calendar_Month]				=	DATEPART(MONTH,Date_Value),
	[Calendar_Month_Name]			=	CAST(DATENAME(MONTH, Date_Value) as VARCHAR(12)),
	[Full_Date_Name]				=	DATENAME(dw, Date_Value) + ', ' +  CAST(DAY(Date_Value) AS VARCHAR(2)) + ' ' + 
										DATENAME(MONTH, Date_Value) + ' ' + CAST(YEAR(Date_Value) AS VARCHAR(4))
	FROM DateCTE a
	ORDER BY Date_Key_Alternate
OPTION (MAXRECURSION 0)




-----------------------------------------
--	POPULATE 'Dim_Feed' DIMENSION TABLE	|
-----------------------------------------

USE Twitter_DB

INSERT INTO Dim_Feed
([Feed_Key], [Feed_Url], [Feed_Description], [Feed_Short_Description])
SELECT 1, 'http://search.twitter.com/search.rss?q=%23MICROSOFT',		'General Microsoft Twitter Category',	'#MICROSOFT'				UNION ALL
SELECT 2, 'http://search.twitter.com/search.rss?q=%23BIGDATA',			'General Big Data Twitter Category', 	'#BIGDATA'					UNION ALL
SELECT 3, 'http://search.twitter.com/search.rss?q=%23MSBI',				'General Microsoft Business Intelligence Twitter Category','#MSBI'	UNION ALL
SELECT 4, 'http://search.twitter.com/search.rss?q=%23MSOFFICE',			'General Microsoft Office Tools Twitter Category', '#MSOFFICE'		UNION ALL
SELECT 5, 'http://search.twitter.com/search.rss?q=%23SHAREPOINT',		'General SharePoint Twitter Category', '#SHAREPOINT'					



-------------------------------------------------------------------------
--	POPULATE ALL DIMENSIONs TABLES WITH UNKNOWN IDENTITY MEMBERS VALUES	|
-------------------------------------------------------------------------
IF EXISTS	(SELECT   c.is_identity
			FROM   sys.tables t
            JOIN sys.schemas s
            ON t.schema_id = s.schema_id
            JOIN sys.Columns c
            ON c.object_id = t.object_id
            JOIN sys.Types ty
            ON ty.system_type_id = c.system_type_id
			WHERE  t.name = 'Dim_Time'
            AND s.Name = 'dbo'
            AND c.is_identity=1)
SET IDENTITY_INSERT dbo.Dim_Time ON

INSERT INTO dbo.Dim_Time
([Time_Key], [Time_Key_Alternate], [Hour], [Minute], [Am_Pm], [Standard_Time])
SELECT 
[Time_Key]					=	-1,
[Time_Key_Alternate]		=	'Unknown',
[Hour]						=	'Unknown',
[Minute]					=	'Unknown',
[Am_Pm]						=	'Unknown',
[Standard_Time]				=	'Unknown'

SET IDENTITY_INSERT dbo.dim_Time OFF	


IF EXISTS	(SELECT   c.is_identity
			FROM   sys.tables t
            JOIN sys.schemas s
            ON t.schema_id = s.schema_id
            JOIN sys.Columns c
            ON c.object_id = t.object_id
            JOIN sys.Types ty
            ON ty.system_type_id = c.system_type_id
			WHERE  t.name = 'Dim_Date'
            AND s.Name = 'dbo'
            AND c.is_identity=1)
SET IDENTITY_INSERT dbo.Dim_Date ON

INSERT INTO dbo.Dim_Date
([Date_Key], [Date_Key_Alternate], [Date_Time], [Calendar_Date], [Week_Day_Name], [Calendar_Year], [Calendar_Quarter], 
[Calendar_Quarter_Name], [Calendar_Month], [Calendar_Month_Name], [Full_Date_Name])
SELECT 
[Date_Key]				=	-1,
[Date_Key_Alternate]	=	99991231,
[Date_Time]				=	CAST('9999-12-31' as DATETIME),
[Calendar_Date]			=	'9999-12-31',
[Week_Day_Name]			=	'Unknown',
[Calendar_Year]			=	-1,
[Calendar_Quarter]		=	-1,
[Calendar_Quarter_Name]	=	'Unknown',
[Calendar_Month]		=	-1,
[Calendar_Month_Name]	=	'Unknown',
[Full_Date_Name]		=	'Unknown'

SET IDENTITY_INSERT dbo.Dim_Date OFF

INSERT INTO dbo.Dim_Feed
([Feed_Key], [Feed_Url], [Feed_Description], [Feed_Short_Description])
SELECT -1, 'Unknown', 'Unknown', 'Unknown'


INSERT INTO dbo.Dim_Sentiment
([Sentiment_Key], [Sentiment])
SELECT -1, 'Unknown'
UNION ALL
SELECT 1, 'Positive'
UNION ALL
SELECT 2, 'Negative'
UNION ALL
SELECT 3, 'Neutral'
