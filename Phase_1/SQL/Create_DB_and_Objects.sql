USE MASTER
GO
IF ( EXISTS( SELECT Name FROM SysDatabases WHERE name = 'Twitter_DB' ) )
BEGIN
ALTER DATABASE [Twitter_DB]    SET SINGLE_USER WITH ROLLBACK IMMEDIATE 
DROP DATABASE [Twitter_DB]
END
GO

CREATE DATABASE Twitter_DB
GO
USE Twitter_DB
GO


-----------------------------------------
--	CREATE 'Dim_Time' DIMENSION TABLE	|
-----------------------------------------

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Dim_Time]') AND type in (N'U'))
DROP TABLE [dbo].[Dim_Time]
GO
CREATE TABLE [dbo].[Dim_Time]
( 
[Time_Key]					[Int]	IDENTITY (1,1)	NOT NULL,
[Time_Key_Alternate]		[varchar]	(12)		NOT NULL, 
[Hour]						[varchar]	(12)		NOT NULL, 
[Minute]					[varchar]	(12)		NOT NULL, 
[Am_Pm]						[varchar]	(12)		NOT NULL, 
[Standard_Time]				[varchar]	(12)		NULL, 

CONSTRAINT [PK_dim_Time] PRIMARY KEY CLUSTERED 
( [Time_Key] ASC)
) ON [PRIMARY] 
GO 
CREATE UNIQUE INDEX Idx_DimTime ON Dim_Time(Time_Key)
GO



-----------------------------------------
--	CREATE 'Dim_Date' DIMENSION TABLE	|
-----------------------------------------

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Dim_Date]') AND type in (N'U'))
DROP TABLE [dbo].[Dim_Date]
GO
CREATE TABLE [dbo].[Dim_Date]
(
[Date_Key]						[int]	IDENTITY (1,1)		NOT NULL,
[Date_Key_Alternate]			[int]						NOT NULL,
[Date_Time]						[datetime]					NOT NULL,
[Calendar_Date]					[date]						NOT NULL,
[Week_Day_Name]					[varchar]	(10)			NOT NULL,
[Calendar_Year]					[int]						NOT NULL,
[Calendar_Quarter]				[int]						NOT NULL,
[Calendar_Quarter_Name]			[varchar]	(16)			NOT NULL,
[Calendar_Month]				[int]						NOT NULL,
[Calendar_Month_Name]			[varchar]	(12)			NOT NULL,
[Full_Date_Name]				[varchar]	(30)			NOT NULL
 
CONSTRAINT [PK_DimDate] PRIMARY KEY CLUSTERED
(
[Date_Key] ASC
)
) ON [PRIMARY]
GO
CREATE UNIQUE INDEX Idx_DimDate ON Dim_Date(Date_Key)
GO



-----------------------------------------
--	CREATE 'Dim_Feed' DIMENSION TABLE	|
-----------------------------------------
	
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Dim_Feed]') AND type in (N'U'))
DROP TABLE [dbo].[Dim_Feed]
GO
CREATE TABLE [dbo].[Dim_Feed]
(
[Feed_Key]					[int]					NOT NULL,
[Feed_URL]					[varchar]	(100)		NOT NULL,
[Feed_Description]			[varchar]	(200)		NOT NULL,
[Feed_Short_Description]	[varchar]	(25)		NOT NULL

CONSTRAINT [PK_Feed] PRIMARY KEY CLUSTERED
(
[Feed_Key] ASC
)
) ON [PRIMARY]
GO
CREATE UNIQUE INDEX Idx_DimFeed ON Dim_Feed(Feed_Key)
GO



---------------------------------------------
--	CREATE 'Dim_Sentiment' DIMENSION TABLE	|
---------------------------------------------

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Dim_Sentiment]') AND type in (N'U'))
DROP TABLE [dbo].[Dim_Sentiment]
GO
CREATE TABLE [dbo].[Dim_Sentiment]
(
[Sentiment_Key]		[Int]				NOT NULL,
[Sentiment]			[varchar]	(30)	NOT NULL
) ON [PRIMARY]
GO
CREATE UNIQUE INDEX Idx_DimSentiment ON Dim_Sentiment (Sentiment_Key)
GO


---------------------------------------------
--	CREATE 'Fact_Twitter_Feeds' FACT TABLE	|
---------------------------------------------

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Fact_Twitter_Feeds]') AND type in (N'U'))
DROP TABLE [dbo].[Fact_Twitter_Feeds]
GO
CREATE TABLE [dbo].[Fact_Twitter_Feeds]
(
[Twitter_ID]		[varchar]	(200)		NOT NULL,
[Twitter_Feed]		[varchar]	(200)		NOT NULL,
[Author]			[varchar]	(100)		NOT NULL,
[Author_Email]		[varchar]	(100)		NOT NULL,
[FK_Sentiment]		[int]					NOT NULL,
[FK_Time]			[int]					NOT NULL,	
[FK_Date]			[int]					NOT NULL,
[FK_Feed]			[int]					NOT NULL
) ON [PRIMARY]

ALTER TABLE [dbo].[Fact_Twitter_Feeds] ADD CONSTRAINT
PK_Fact_Twitter_Feeds PRIMARY KEY ([Twitter_ID],[Twitter_Feed], [Author], [Author_Email])

ALTER TABLE [Fact_Twitter_Feeds] ADD CONSTRAINT 
FK_Fact_Twitter_Feeds_Dim_Sentiment FOREIGN KEY ([FK_Sentiment])
REFERENCES  [dbo].[Dim_Sentiment] ([Sentiment_Key])

ALTER TABLE [Fact_Twitter_Feeds] ADD CONSTRAINT 
FK_Fact_Twitter_Feeds_Dim_Date FOREIGN KEY ([FK_Date])
REFERENCES  [dbo].[Dim_Date] ([Date_Key])

ALTER TABLE [Fact_Twitter_Feeds] ADD CONSTRAINT 
FK_Fact_Twitter_Feeds_Dim_Feed FOREIGN KEY ([FK_Feed])
REFERENCES  [dbo].[Dim_Feed] ([Feed_Key])

ALTER TABLE [Fact_Twitter_Feeds] ADD CONSTRAINT 
FK_Fact_Twitter_Feeds_Dim_Time FOREIGN KEY ([FK_Time])
REFERENCES  [dbo].[Dim_Time] ([Time_Key])



---------------------------------------------
--	CREATE 'Ctl_Sentiment' LOOK-UP TABLE	|
---------------------------------------------
	
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Ctl_Sentiment]') AND type in (N'U'))
DROP TABLE [dbo].[Ctl_Sentiment]
GO
CREATE TABLE [dbo].[Ctl_Sentiment]
(
[Sentiment_ID]		[int] IDENTITY (1,1)	NOT NULL,
[Term]				[varchar]	(30)		NOT NULL,
[Term_Category]		[varchar]	(30)		NOT NULL

CONSTRAINT [PK_Sentiment] PRIMARY KEY CLUSTERED
(
[Sentiment_ID] ASC
)
) ON [PRIMARY]
GO
CREATE UNIQUE INDEX Idx_CtlSentiment ON Ctl_Sentiment (Sentiment_ID)
GO



---------------------------------------------------------
--	CREATE 'Twitter_Feeds_Staging_Data' STAGING TABLE	|
---------------------------------------------------------

CREATE TABLE [dbo].[Twitter_Feeds_Staging_Data]
(
[Twitter_ID]		[varchar]	(200)		NOT NULL,
[Twitter_Feed]		[varchar]	(200)		NOT NULL,
[Twitter_Date]		[datetime]				NOT NULL,
[Feed_ID]			[int]					NOT NULL,
[Author_Details]	[varchar]	(200)		NOT NULL,
[Author_Email]		[varchar]	(100)		NULL,
[Author]			[varchar]	(100)		NULL,
[Sentiment]			[varchar]	(30)		NULL
) ON [PRIMARY]
GO
CREATE UNIQUE INDEX Idx_TW_ID ON Twitter_Feeds_Staging_Data(Twitter_ID, Feed_ID)
GO