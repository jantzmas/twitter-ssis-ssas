Original tutorial that this example is based off of:
http://bicortex.com/twitter-data-analysis-using-microsoft-sql-server-c-ssis-ssas-and-excel/

Tasks to build the Twitter Project:

Task 1: Create and populate a new database named Twitter_DB
The files necessary for this task can be found within the cloned Project in the SQL file.  
Connect to your desired database engine in SQL Server Management Studio (SSMS).

First, open the Create_DB_and_Objects.sql file in a new query editor panel in SSMS and run it.  This will create the Twitter_DB database and populate it with 7 tables as well as two stored procedures.  

Second, open the Populate_Objects.sql file in a new query editor panel in SSMS and run it.  This will populate the Dim_Date, Dim_Sentiment, and Dim_Time tables appropriately. 
 


Task 2: Set up your SSAS Solution and Deploy your Cube
Open up the Twitter_SSAS.sln file in Visual Studio and in the solution explorer, you will first need to update the datasource, Twitter_DB.ds, to point to the correct location.  Hence, double click it or right click on it, and select open.  In the 'General' tab of the Data Source Designer that pops up, press the Edit button under the connection string and ensure you are pointed/connected to your Twitter_DB instance.  You will also want to change the username/password information on the 'Impersonation Information' tab.  

Now we will deploy our cube by right clicking on the Twitter_SSAS solution in the Solution Explorer and selecting 'Deploy'.



Task 3: Set up your SSIS Solution
Open up the Twitter_SSIS.sln file in Visual Studio and notice the two packages (Terms_Extraction.dtsx and Twitter_SSIS.dtsx) it contains.  Before you try to execute either of these packages, you must appropriately set the Connection Managers to correctly point to your Twitter_DB instance and the Twitter_SSAS cube. Do this by double clicking on each connection manager and clicking the edit button in the resulting popup to alter the connection strings.  

Open up the Terms_Extraction.dtsx project by double clicking on it and notice you will also have to ensure the Positive and Negative file connections that you now see in the connection manager point to the appropriate files that were bundled with this project in the Text_Files folder.  

Right click on the Terms_Extraction.dtsx project and select 'Execute Package'.  This will populate the Ctl_Sentiment table in your Twitter_DB instance.  

Now, the Twitter_SSIS.dtsx package and observe the Data Flow for the 'Pull in Twitter Data' task.  In this data flow, you will need to double click the 'Extract Feed Data From Twitter' item and press the Edit Script button.  Within this script which extracts tweets by calling the Twitter API, you will need to enter Twitter Developer tokens to access the Twitter API with.



Task 4: Populate the Cube
To set the terms you will be extracting tweets based on, you will have to manually add items to the Dim_Search_Term table in your Twitter_DB database.

Having set the term(s) that you desire to search for and analyze the tweets of, right click on the Twitter_SSIS.dtsx Package in the Twitter_SSIS project in Visual Studio and select 'Execute Package'.  If all runs smoothly, you will now have resulting data in the Fact_Twitter_Feeds database table as well as populated in your cube!!



Task 5: Have fun analyzing your collected data!!
Don't just sit around with your nicely collected data!  Explore it using Excel, Power BI, tableau, or any other tools you desire!
