USE [master]
GO
/****** Object:  Database [Twitter_DB]    Script Date: 3/4/2015 11:23:04 AM ******/
CREATE DATABASE [Twitter_DB]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'Twitter_DB', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL11.MSSQLSERVER\MSSQL\DATA\Twitter_DB.mdf' , SIZE = 6208KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'Twitter_DB_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL11.MSSQLSERVER\MSSQL\DATA\Twitter_DB_log.ldf' , SIZE = 18560KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [Twitter_DB] SET COMPATIBILITY_LEVEL = 110
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [Twitter_DB].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [Twitter_DB] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [Twitter_DB] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [Twitter_DB] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [Twitter_DB] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [Twitter_DB] SET ARITHABORT OFF 
GO
ALTER DATABASE [Twitter_DB] SET AUTO_CLOSE ON 
GO
ALTER DATABASE [Twitter_DB] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [Twitter_DB] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [Twitter_DB] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [Twitter_DB] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [Twitter_DB] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [Twitter_DB] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [Twitter_DB] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [Twitter_DB] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [Twitter_DB] SET  ENABLE_BROKER 
GO
ALTER DATABASE [Twitter_DB] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [Twitter_DB] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [Twitter_DB] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [Twitter_DB] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [Twitter_DB] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [Twitter_DB] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [Twitter_DB] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [Twitter_DB] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [Twitter_DB] SET  MULTI_USER 
GO
ALTER DATABASE [Twitter_DB] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [Twitter_DB] SET DB_CHAINING OFF 
GO
ALTER DATABASE [Twitter_DB] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [Twitter_DB] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
USE [Twitter_DB]
GO
/****** Object:  UserDefinedFunction [dbo].[Split]    Script Date: 3/4/2015 11:23:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[Split] (@sep VARCHAR(32), @s VARCHAR(MAX))

RETURNS @t TABLE
    (
        val VARCHAR(MAX)
    )   
AS
    BEGIN
        DECLARE @xml XML
        SET @XML = N'<root><r>' + REPLACE(@s, @sep, '</r><r>') + '</r></root>'

        INSERT INTO @t(val)
        SELECT r.value('.','VARCHAR(50)') as Item
        FROM @xml.nodes('//root/r') AS RECORDS(r)
        RETURN
    END

GO
/****** Object:  Table [dbo].[Ctl_Sentiment]    Script Date: 3/4/2015 11:23:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Ctl_Sentiment](
	[Sentiment_ID] [int] IDENTITY(1,1) NOT NULL,
	[Term] [varchar](30) NOT NULL,
	[Term_Category] [varchar](30) NOT NULL,
 CONSTRAINT [PK_Sentiment] PRIMARY KEY CLUSTERED 
(
	[Sentiment_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Dim_Date]    Script Date: 3/4/2015 11:23:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Dim_Date](
	[Date_Key] [int] IDENTITY(1,1) NOT NULL,
	[Date_Key_Alternate] [int] NOT NULL,
	[Date_Time] [datetime] NOT NULL,
	[Calendar_Date] [date] NOT NULL,
	[Week_Day_Name] [varchar](10) NOT NULL,
	[Calendar_Year] [int] NOT NULL,
	[Calendar_Quarter] [int] NOT NULL,
	[Calendar_Quarter_Name] [varchar](16) NOT NULL,
	[Calendar_Month] [int] NOT NULL,
	[Calendar_Month_Name] [varchar](12) NOT NULL,
	[Full_Date_Name] [varchar](30) NOT NULL,
 CONSTRAINT [PK_DimDate] PRIMARY KEY CLUSTERED 
(
	[Date_Key] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Dim_Search_Term]    Script Date: 3/4/2015 11:23:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Dim_Search_Term](
	[Term_Key] [int] IDENTITY(1,1) NOT NULL,
	[Term] [nvarchar](50) NULL,
 CONSTRAINT [PK__Dim_Search_Term_Feed_Term] PRIMARY KEY CLUSTERED 
(
	[Term_Key] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Dim_Sentiment]    Script Date: 3/4/2015 11:23:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Dim_Sentiment](
	[Sentiment_Key] [int] NOT NULL,
	[Sentiment] [varchar](30) NOT NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Dim_Time]    Script Date: 3/4/2015 11:23:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Dim_Time](
	[Time_Key] [int] IDENTITY(1,1) NOT NULL,
	[Time_Key_Alternate] [varchar](12) NOT NULL,
	[Hour] [varchar](12) NOT NULL,
	[Minute] [varchar](12) NOT NULL,
	[Am_Pm] [varchar](12) NOT NULL,
	[Standard_Time] [varchar](12) NULL,
 CONSTRAINT [PK_dim_Time] PRIMARY KEY CLUSTERED 
(
	[Time_Key] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Fact_Twitter_Feeds]    Script Date: 3/4/2015 11:23:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Fact_Twitter_Feeds](
	[Twitter_ID] [varchar](200) NOT NULL,
	[Twitter_Feed] [varchar](200) NOT NULL,
	[Author] [varchar](100) NOT NULL,
	[Author_Email] [varchar](100) NOT NULL,
	[FK_Sentiment] [int] NOT NULL,
	[Sentiment_Magnitude] [int] NOT NULL,
	[FK_Time] [int] NOT NULL,
	[FK_Date] [int] NOT NULL,
	[FK_Search_Term] [int] NOT NULL,
	[Latitude] [float] NULL,
	[Longitude] [float] NULL,
	[Location] [varchar](200) NULL,
 CONSTRAINT [PK_Fact_Twitter_Feeds] PRIMARY KEY CLUSTERED 
(
	[Twitter_ID] ASC,
	[Twitter_Feed] ASC,
	[Author] ASC,
	[Author_Email] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Twitter_Feeds_Staging_Data]    Script Date: 3/4/2015 11:23:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Twitter_Feeds_Staging_Data](
	[Twitter_ID] [varchar](200) NOT NULL,
	[Twitter_Feed] [varchar](200) NOT NULL,
	[Twitter_Date] [datetime] NOT NULL,
	[Search_Term_ID] [int] NOT NULL,
	[Author_Details] [varchar](200) NOT NULL,
	[Author_Email] [varchar](100) NULL,
	[Author] [varchar](100) NULL,
	[Sentiment_Result] [int] NULL,
	[Sentiment_Magnitude] [int] NULL,
	[Tweet_ID] [int] IDENTITY(1,1) NOT NULL,
	[Latitude] [float] NULL,
	[Longitude] [float] NULL,
	[Location] [varchar](200) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Index [Idx_CtlSentiment]    Script Date: 3/4/2015 11:23:04 AM ******/
CREATE UNIQUE NONCLUSTERED INDEX [Idx_CtlSentiment] ON [dbo].[Ctl_Sentiment]
(
	[Sentiment_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [Idx_DimDate]    Script Date: 3/4/2015 11:23:04 AM ******/
CREATE UNIQUE NONCLUSTERED INDEX [Idx_DimDate] ON [dbo].[Dim_Date]
(
	[Date_Key] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [Idx_DimSentiment]    Script Date: 3/4/2015 11:23:04 AM ******/
CREATE UNIQUE NONCLUSTERED INDEX [Idx_DimSentiment] ON [dbo].[Dim_Sentiment]
(
	[Sentiment_Key] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [Idx_DimTime]    Script Date: 3/4/2015 11:23:04 AM ******/
CREATE UNIQUE NONCLUSTERED INDEX [Idx_DimTime] ON [dbo].[Dim_Time]
(
	[Time_Key] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [Idx_TW_ID]    Script Date: 3/4/2015 11:23:04 AM ******/
CREATE UNIQUE NONCLUSTERED INDEX [Idx_TW_ID] ON [dbo].[Twitter_Feeds_Staging_Data]
(
	[Twitter_ID] ASC,
	[Search_Term_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Twitter_Feeds_Staging_Data] ADD  CONSTRAINT [DF_Twitter_Feeds_Staging_Data_Sentiment]  DEFAULT ((0)) FOR [Sentiment_Result]
GO
ALTER TABLE [dbo].[Twitter_Feeds_Staging_Data] ADD  CONSTRAINT [DF_Twitter_Feeds_Staging_Data_Sentiment_Magnitude]  DEFAULT ((0)) FOR [Sentiment_Magnitude]
GO
ALTER TABLE [dbo].[Fact_Twitter_Feeds]  WITH CHECK ADD  CONSTRAINT [FK_Fact_Twitter_Feeds_Dim_Date] FOREIGN KEY([FK_Date])
REFERENCES [dbo].[Dim_Date] ([Date_Key])
GO
ALTER TABLE [dbo].[Fact_Twitter_Feeds] CHECK CONSTRAINT [FK_Fact_Twitter_Feeds_Dim_Date]
GO
ALTER TABLE [dbo].[Fact_Twitter_Feeds]  WITH CHECK ADD  CONSTRAINT [FK_Fact_Twitter_Feeds_Dim_Search_Term] FOREIGN KEY([FK_Search_Term])
REFERENCES [dbo].[Dim_Search_Term] ([Term_Key])
GO
ALTER TABLE [dbo].[Fact_Twitter_Feeds] CHECK CONSTRAINT [FK_Fact_Twitter_Feeds_Dim_Search_Term]
GO
ALTER TABLE [dbo].[Fact_Twitter_Feeds]  WITH CHECK ADD  CONSTRAINT [FK_Fact_Twitter_Feeds_Dim_Sentiment] FOREIGN KEY([FK_Sentiment])
REFERENCES [dbo].[Dim_Sentiment] ([Sentiment_Key])
GO
ALTER TABLE [dbo].[Fact_Twitter_Feeds] CHECK CONSTRAINT [FK_Fact_Twitter_Feeds_Dim_Sentiment]
GO
ALTER TABLE [dbo].[Fact_Twitter_Feeds]  WITH CHECK ADD  CONSTRAINT [FK_Fact_Twitter_Feeds_Dim_Time] FOREIGN KEY([FK_Time])
REFERENCES [dbo].[Dim_Time] ([Time_Key])
GO
ALTER TABLE [dbo].[Fact_Twitter_Feeds] CHECK CONSTRAINT [FK_Fact_Twitter_Feeds_Dim_Time]
GO
/****** Object:  StoredProcedure [dbo].[usp_Populate_Fact_Twitter_Feeds]    Script Date: 3/4/2015 11:23:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_Populate_Fact_Twitter_Feeds]
AS 
    BEGIN
        BEGIN TRY

            INSERT  INTO [Twitter_DB].[dbo].[Fact_Twitter_Feeds]
                    ( Twitter_ID ,
                      Twitter_Feed ,
                      Author ,
                      Author_Email ,
                      FK_Sentiment ,
					  Sentiment_Magnitude,
                      FK_Time ,
                      FK_Date ,
                      FK_Search_Term,
					  Latitude,
					  Longitude,
					  Location
                    )
                    SELECT  tfsd.Twitter_ID ,
                            tfsd.Twitter_Feed ,
                            tfsd.Author ,
                            tfsd.Author_Email ,
                            ISNULL(s.Sentiment_Key, -1) ,
							tfsd.Sentiment_Magnitude ,
                            ISNULL(t.Time_Key, -1) ,
                            ISNULL(d.Date_Key, -1) ,
                            ISNULL(f.Term_Key, -1), --tfsd.Search_Term_ID,
							tfsd.Latitude,
							tfsd.Longitude,
							tfsd.Location
                    FROM    [Twitter_DB].[dbo].[Twitter_Feeds_Staging_Data] AS tfsd
                            LEFT JOIN [Twitter_DB].[dbo].[Dim_Sentiment] s ON tfsd.Sentiment_Result = s.Sentiment_Key
                            LEFT JOIN [Twitter_DB].[dbo].[Dim_Time] t ON SUBSTRING(CONVERT(VARCHAR, CONVERT(TIME, tfsd.Twitter_Date), 108),
                                                              0, 6) + ':00' = t.Time_Key_Alternate
                            LEFT JOIN [Twitter_DB].[dbo].[Dim_Date] d ON CAST(CONVERT(CHAR(8), CAST(tfsd.Twitter_Date AS DATETIME), 112) AS INT) = d.Date_Key_Alternate
                            LEFT JOIN [Twitter_DB].[dbo].[Dim_Search_Term] f ON tfsd.Search_Term_ID = f.Term_Key
                    WHERE   NOT EXISTS ( SELECT TOP 1
                                                'I already exist'
                                         FROM   [Twitter_DB].[dbo].[Fact_Twitter_Feeds] fact
                                         WHERE  fact.Twitter_ID = tfsd.Twitter_ID
                                                AND fact.Twitter_Feed = tfsd.Twitter_Feed
                                                AND fact.Author = tfsd.Author
                                                AND fact.Author_Email = tfsd.Author_Email )
        END TRY

        BEGIN CATCH
            IF @@TRANCOUNT > 0 
                BEGIN
                    ROLLBACK TRANSACTION
                END	

            DECLARE @ErrorMessage NVARCHAR(4000);
            DECLARE @ErrorSeverity INT;
            DECLARE @ErrorState INT;

            SELECT  @ErrorMessage = ERROR_MESSAGE() ,
                    @ErrorSeverity = ERROR_SEVERITY() ,
                    @ErrorState = ERROR_STATE();
			
            RAISERROR (@ErrorMessage, -- Message text.
				   @ErrorSeverity, -- Severity.
				   @ErrorState -- State.
				   );
        END CATCH

        IF @@TRANCOUNT > 0 
            BEGIN
                COMMIT TRANSACTION
            END
    END

GO
/****** Object:  StoredProcedure [dbo].[usp_Update_Twitter_Feeds_Staging_Data]    Script Date: 3/4/2015 11:23:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_Update_Twitter_Feeds_Staging_Data] AS

BEGIN
BEGIN TRY

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[#Email]') AND type in (N'U'))
DROP TABLE [dbo].[#Email];

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[#Author]') AND type in (N'U'))
DROP TABLE [dbo].[#Author];

--IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[#Sentiment]') AND type in (N'U'))
--DROP TABLE [dbo].[#Sentiment];


--Extract E-mail addresses
SELECT  Twitter_ID, CASE WHEN AtIndex=0 THEN '' --no email found 
           ELSE RIGHT(head, PATINDEX('% %', REVERSE(head) + ' ') - 1) 
        + LEFT(tail + ' ', PATINDEX('% %', tail + ' ')) 
        END Author_Email
INTO #Email
FROM (SELECT Twitter_ID,RIGHT(EmbeddedEmail, [len] - AtIndex) AS tail,
             LEFT(EmbeddedEmail, AtIndex) AS head, AtIndex
			 FROM (SELECT Twitter_ID,PATINDEX('%[A-Z0-9]@[A-Z0-9]%', EmbeddedEmail+' ') AS AtIndex,
					LEN(EmbeddedEmail+'|')-1 AS [len],
					EmbeddedEmail
					FROM   (SELECT Twitter_ID, Author_Details,Author_Email from [Twitter_DB].[dbo].[Twitter_Feeds_Staging_Data])
					AS a (Twitter_ID,EmbeddedEmail,Author_Email) WHERE a.[Author_Email] IS NULL
           )a
     )b


--Extract Author Names
SELECT Twitter_ID, CASE WHEN CHARINDEX ('(', Author_Details)>1 THEN
REPLACE(SUBSTRING (Author_Details, CHARINDEX ('(', Author_Details,0) +1, LEN(Author_Details)),')','') END AS Author
INTO #Author
FROM [Twitter_DB].[dbo].[Twitter_Feeds_Staging_Data]
WHERE [Author] IS NULL



--Extract Sentiment
--DECLARE @0 nvarchar (1) SET @0 = '' 
--DECLARE @1 nvarchar (1) SET @1 = '>' 
--DECLARE @2 nvarchar (1) SET @2 = '<' 
--DECLARE @3 nvarchar (1) SET @3 = '('
--DECLARE @4 nvarchar (1) SET @4 = ')'
--DECLARE @5 nvarchar (1) SET @5 = '!'
--DECLARE @6 nvarchar (1) SET @6 = '?'
--DECLARE @7 nvarchar (1) SET @7 = ','
--DECLARE @8 nvarchar (1) SET @8 = '@'
--DECLARE @9 nvarchar (1) SET @9 = '#'
--DECLARE @10 nvarchar (1) SET @10 = '$'
--DECLARE @11 nvarchar (1) SET @11 = '%'
--DECLARE @12 nvarchar (1) SET @12 = '&';      

--WITH temp_results as 
--	(
--	SELECT  tfsd.Twitter_ID, 
--	upper(ltrim(rtrim(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace
--	(fs.val,@1,@0),@2,@0),@3,@0),@4,@0),@5,@0),@6,@0),@7,@0),@8,@0),@9,@0),@10,@0),@11,@0),@12,@0)))) as val, se.Term_Category
--	FROM [Twitter_DB].[dbo].[Twitter_Feeds_Staging_Data] tfsd
--	CROSS APPLY dbo.Split(' ',tfsd.Twitter_Feed) as fs  
--	LEFT JOIN Ctl_Sentiment se on 
--	upper(ltrim(rtrim(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace
--	(fs.val,@1,@0),@2,@0),@3,@0),@4,@0),@5,@0),@6,@0),@7,@0),@8,@0),@9,@0),@10,@0),@11,@0),@12,@0))))
--	= Upper(se.term)
--	WHERE tfsd.Sentiment IS NULL
--	) 
--SELECT Twitter_ID, Term_Category as Sentiment 
--INTO #Sentiment
--FROM
--	(SELECT  Twitter_ID, Term_Category,rnk FROM 
--		(SELECT Counts, Twitter_ID, Term_Category, RANK() OVER (PARTITION BY Twitter_ID ORDER BY Counts DESC) AS rnk FROM	
--			(SELECT COUNT(Term_Category) Counts, Twitter_ID, Term_Category FROM
--				temp_results 
--				GROUP BY Twitter_ID, Term_Category
--			) a
--		)b
--	where b.rnk = 1) c			

--Update Twitter_Feeds_Staging_Data
UPDATE [Twitter_DB].[dbo].[Twitter_Feeds_Staging_Data]
SET 
Author_Email	=	CASE WHEN b.Author_Email = '' THEN 'Unknown' ELSE b.Author_Email END,
Author			=	ISNULL(c.Author,'Unknown')
--Sentiment		=	ISNULL(d.Sentiment,'Neutral')
FROM [Twitter_DB].[dbo].[Twitter_Feeds_Staging_Data] a
LEFT JOIN #Email b		ON a.Twitter_ID = b.Twitter_ID
LEFT JOIN #Author c		ON a.Twitter_ID = c.Twitter_ID 
--LEFT JOIN #Sentiment d	ON a.Twitter_ID = d.Twitter_ID
WHERE a.Author_Email IS NULL OR a.Author IS NULL-- OR a.Sentiment IS NULL
OR a.Author_Email = '' OR a.Author = ''-- OR a.Sentiment = ''



--Delete Duplicate Twitter Feeds
DECLARE @ID varchar (200)
DECLARE @COUNT int
DECLARE CUR_DELETE CURSOR FOR
	SELECT [Twitter_ID],COUNT([Twitter_ID]) FROM [Twitter_DB].[dbo].[Twitter_Feeds_Staging_Data] 
	GROUP BY [Twitter_ID] HAVING COUNT([Twitter_ID]) > 1

	OPEN CUR_DELETE

		FETCH NEXT FROM CUR_DELETE INTO @ID, @COUNT
		WHILE @@FETCH_STATUS = 0
		BEGIN
		DELETE TOP(@COUNT -1) FROM [Twitter_DB].[dbo].[Twitter_Feeds_Staging_Data] WHERE [Twitter_ID] = @ID
		FETCH NEXT FROM CUR_DELETE INTO @ID, @COUNT
	END
CLOSE CUR_DELETE
DEALLOCATE CUR_DELETE


END TRY

BEGIN CATCH
		IF @@TRANCOUNT > 0
		BEGIN
			ROLLBACK TRANSACTION
		END	

		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT 
			@ErrorMessage = ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();
			
		RAISERROR (@ErrorMessage, -- Message text.
				   @ErrorSeverity, -- Severity.
				   @ErrorState -- State.
				   );
END CATCH

	IF @@TRANCOUNT > 0
	BEGIN
		COMMIT TRANSACTION
	END
END





GO
USE [master]
GO
ALTER DATABASE [Twitter_DB] SET  READ_WRITE 
GO
